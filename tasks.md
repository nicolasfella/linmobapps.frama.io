# Tasks

## Easy tasks, great for first-time contributions:

* Check if apps are available on Arch, Mobian or Flathub.
* If you are running another distribution, feel free to add an extra column for it and start filling it.
* add a new app, e.g.:
  * Navit
  * FoxtrotGPS
  * maliit2
  * Axolotl (if it actually works outside Ubuntu Touch)
  * Gnome Photos
  * Gthumb
  * Shotwell // apps from here on where spotted on a screenshot
  * Corebird (if still actively developed, otherwise Cawbird should be plenty)
  * Claws Mail
  * D-Feet
  * Gpredict
  * KeePassXC
  * KiCad
  * Shadowsocks-Qt5
  * Stellarium 

## Content tasks:

* Remove apps that are no longer available (e.g. source code gone) by copying them to archive.csv.
* Branch out games to games.csv, create a games.html page and link that on index.html.

## Design tasks:

* ~~Make the top menu work on mobile.~~
* Improve mobile design generally.
* Implement a way to display screenshots and or app logos.
* Implement a form with the necessary fields for apps.csv generation for easier email submission.
